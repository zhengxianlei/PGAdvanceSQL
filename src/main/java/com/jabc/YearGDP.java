package com.jabc;

public class YearGDP {
    private String country_name;
    private String country_code;
    private String indicator_name;
    private String indicator_code;
    private int year;
    private double gdp;

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getIndicator_name() {
        return indicator_name;
    }

    public void setIndicator_name(String indicator_name) {
        this.indicator_name = indicator_name;
    }

    public String getIndicator_code() {
        return indicator_code;
    }

    public void setIndicator_code(String indicator_code) {
        this.indicator_code = indicator_code;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getGdp() {
        return gdp;
    }

    public void setGdp(double gdp) {
        this.gdp = gdp;
    }

    @Override
    public String toString() {
        return "YearGDP{" +
                "country_name='" + country_name + '\'' +
                ", country_code='" + country_code + '\'' +
                ", indicator_name='" + indicator_name + '\'' +
                ", indicator_code='" + indicator_code + '\'' +
                ", year=" + year +
                ", gdp=" + gdp +
                '}';
    }
}
