create table country_gdp_year (country_name text,country_code text,indicator_name text,indicator_code text,year int,gdp numeric);
comment on table country_gdp_year is '世界各国家地区1960~2018 GDP';
comment on column country_gdp_year.country_name is '国家名称';
comment on column country_gdp_year.country_code is '国家代码';
comment on column country_gdp_year.indicator_name is '指标名称';
comment on column country_gdp_year.indicator_code is '指标代码';
comment on column country_gdp_year.year is '年份';
comment on column country_gdp_year.gdp is 'GDP产值';

create table metadata_country(country_code text,region text,income_group text);
comment on table metadata_country is '世界各国家地区元信息';
comment on column metadata_country.country_code is '国家代码';
comment on column metadata_country.region is '所属地区';
comment on column metadata_country.income_group is '收入群体';
