
create table country_gdp_year_final as
select gdp.country_code,gdp.country_name,c.income_group,c.region,"year",gdp.gdp from  country_gdp_year gdp
INNER  join  metadata_country c on c.country_code=gdp.country_code
where   region is not null and income_group is not null
order by  gdp.country_code asc,"year" desc